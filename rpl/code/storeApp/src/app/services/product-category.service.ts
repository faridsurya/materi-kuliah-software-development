import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

//data model untuk product category
interface ProductCategory{
  id: string,
  name:string
}
@Injectable({
  providedIn: 'root'
})
export class ProductCategoryService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  async getAll() {
    let data: any = [];
    let products = await this.firestore
      .collection<ProductCategory>('product_categories')
      .get()
      .toPromise();
    products.forEach((val) => {
      let dataValue = {
        id: val.id,
        name: val.data().name       
      };
      data.push(dataValue);
    });
    return data;
  }

}
